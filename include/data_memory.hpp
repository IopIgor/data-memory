#ifndef DATA_MEMORY_HPP
#define DATA_MEMORY_HPP

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(DM) 
{
  static const unsigned bit_addr = 32;
  static const unsigned bit_data = 32;
  static const unsigned MEM_SIZE = 1024; // theoretical (2^32 /4), but goes to overflow

  sc_in<sc_bv<bit_addr> > address;
  sc_in<bool> mem_read;
  sc_in<bool> mem_write;
  sc_in<sc_bv<bit_data> > write_data;
  sc_out<sc_bv<bit_data> > read_data;
  
  SC_CTOR(DM) 
  {
    SC_THREAD(behav);
      sensitive << address << mem_read << mem_write << write_data;

    for (unsigned i=0; i<MEM_SIZE; i++) //memory intialized to 0
      locations[i]=0;
  }

  sc_bv<bit_data> Loc(unsigned i) const {return locations[i];} 
  
 private:
  sc_bv<bit_data> locations[MEM_SIZE]; 

  void behav();
};

#endif
