#include <systemc.h>
#include <iostream>
#include "data_memory.hpp"

using namespace std;
using namespace sc_core;

const sc_time wait_time(1, SC_NS);
const unsigned bit_addr = 32;
const unsigned bit_data = 32;

SC_MODULE(TestBench) 
{
 public:
  sc_signal<sc_bv<bit_addr> > address;
  sc_signal<bool> mem_read;
  sc_signal<bool> mem_write;
  sc_signal<sc_bv<bit_data> > write_data;
  sc_signal<sc_bv<bit_data> > read_data;

  DM memo;

  SC_CTOR(TestBench) : memo("memo")
  {
    SC_THREAD(stimulus_thread);
    SC_THREAD(watcher_thread);
      sensitive << address << mem_read << mem_write << write_data;

    memo.address(this->address);
    memo.mem_read(this->mem_read);
    memo.mem_write(this->mem_write);
	  memo.write_data(this->write_data);
	  memo.read_data(this->read_data);
  }

  bool check() 
  {
    error = 0;
    if(read_data.read() != write_data.read())
    {
      cout << "test fallito!! Valore scritto: " << write_data.read() << "; valore letto: " 
           << read_data.read() << endl;
      error = 1;
    }
    
    return error;
  }
  
 private:
  bool error;

  void stimulus_thread() 
  {    
    //read the address 15 (result expected = 0)
    address.write(15);
    mem_read.write(true);
    wait(wait_time);

    //write 20 in address 15
    mem_read.write(false);
    write_data.write(20);
    mem_write.write(true);
    wait(wait_time);

    //read the address 15 (result expected = 20)
    mem_write.write(false);
    mem_read.write(true);
    wait(wait_time);
  }

  void watcher_thread()
  {
    while(true)
    {
      wait();
      wait(1, SC_PS);
      if (mem_write.read())
         cout << "At time " << sc_time_stamp() << ": writed " << write_data.read().to_uint()
              << " in address " << address.read().to_uint() << " in data memory" << endl;

      if (mem_read.read())
         cout << "At time " << sc_time_stamp() << ": on the output of data memory at address " 
              << address.read().to_uint() << " there is " 
              << memo.Loc(address.read().to_uint()).to_uint() << endl;
    }
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.check();
}

