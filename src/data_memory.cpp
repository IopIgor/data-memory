#include <systemc.h>
#include "data_memory.hpp"

using namespace std;
using namespace sc_core;

void DM::behav()
{
   while(true)
   {
      wait();
      if (mem_write->read())
         locations[address->read().to_uint()]= write_data->read();

      if (mem_read->read())
         read_data->write(locations[address->read().to_uint()]);
   }
}
